# Uses cases and affordances, Issue 1.2

## Index
* **Getting used to misuse**, ginger coon — *Editor's letter*
* **Version under control**, Manufactura Independente — *Production colophon*
* *New releases*
* *Upcoming events*
* **Copyleft business**, Dave Crossland — *Column*
* **The heritage of our pixels**, Eric Schrijver — *Column*
* **Coding pictures**, Ricardo Lafuente — *Dispatch*
* **Setting a book with Scribus**, Pierre Marchand — *First time*
* **Wayfinding and warnings from Wikimedia Commons** — *Best of SVG*
* **Desktop**, Pierros Papadeas — *Hacks*
* **Papercut**, Allison Moore — *Showcase*
* **What revolution**, Antonio Roberts — *Showcase*
* **PropCourier Sans: we're not here to be correct** — *Specimen*
* **Making your workflow work for you**, Seth Kenlon — *Feature*
* **AdaptableGIMP: user interfaces for users**, ginger coons — *Feature*
* *Resource list*
* *Glossary*

## Colophon

Uses cases and affordances — February 2011   
Issue 1.2, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416